/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect, useState} from 'react';
import {NativeRouter, Route, Routes} from 'react-router-native';
import {
  Button,
  MD3DarkTheme,
  MD3LightTheme,
  Provider as PaperProvider,
  Text,
} from 'react-native-paper';
import {setI18nConfig, translate} from './utils/localize';
import MangaView from './src/pages/MangaView';
import MangaIntegrityView from './src/pages/MangaIntegrityView';
import ChapterView from './src/pages/ChapterView';
import Homepage from './src/pages/Homepage';
import {Alert, ToastAndroid, useColorScheme} from 'react-native';
import MusicBoxView from './src/pages/MusicBoxView';
import DocumentPicker from 'react-native-document-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getSoundMangaPackDirectory} from './utils/preferences';
import Layout from './src/components/Layout';
import Clipboard from '@react-native-community/clipboard';

const lightTheme = {
  ...MD3LightTheme,
  dark: false,
  colors: {
    ...MD3LightTheme.colors,
  },
};

const darkTheme = {
  ...MD3DarkTheme,
  dark: true,
  colors: {
    ...MD3DarkTheme.colors,
  },
};

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';
  setI18nConfig();

  const [soundMangaDirectory, setSoundMangaDirectory] = useState<string | null>(
    null,
  );
  const [isLoaded, setLoaded] = useState<boolean>(false);

  const pickDirectory = () => {
    DocumentPicker.pickDirectory().then(async r => {
      const externalStorageDirectoryUri =
        'content://com.android.externalstorage.documents/tree/primary:';
      if (r && r.uri) {
        if (decodeURIComponent(r.uri).startsWith(externalStorageDirectoryUri)) {
          const selectedDir = decodeURIComponent(r.uri).split(
            externalStorageDirectoryUri,
          )[1];

          const fullDirName = decodeURIComponent(selectedDir);
          await AsyncStorage.setItem('@SMR_directory', fullDirName);
          updateSoundMangaDirectory();
        } else {
          Alert.alert(
            'Unexpected directory value',
            'Please show this value to the app developer: ' +
              decodeURIComponent(r.uri),
            [
              {
                text: 'Copy',
                onPress: () => {
                  Clipboard.setString(decodeURIComponent(r.uri));
                  ToastAndroid.show('Value copied to clipboard', 2);
                },
              },
              {
                text: 'OK',
              },
            ],
          );
        }
      }
    });
  };

  const updateSoundMangaDirectory = () => {
    getSoundMangaPackDirectory(false)
      .then(smrDir => {
        setSoundMangaDirectory(smrDir);
        setLoaded(true);
      })
      .catch(e => {
        setSoundMangaDirectory(null);
        setLoaded(true);
      });
  };

  useEffect(() => {
    updateSoundMangaDirectory();
  }, []);

  return (
    <PaperProvider theme={isDarkMode ? darkTheme : lightTheme}>
      {soundMangaDirectory !== null ? (
        <NativeRouter>
          <Routes>
            <Route
              path="/"
              element={<Homepage packsDir={soundMangaDirectory} />}
            />
            <Route
              path="/manga/:manga"
              element={<MangaView packsDir={soundMangaDirectory} />}
            />
            <Route
              path="/manga/:manga/integrity"
              element={<MangaIntegrityView packsDir={soundMangaDirectory} />}
            />
            <Route
              path="/manga/:manga/chapter/:chapter"
              element={<ChapterView packsDir={soundMangaDirectory} />}
            />
            <Route
              path="/musicbox/:folder"
              element={<MusicBoxView packsDir={soundMangaDirectory} />}
            />
          </Routes>
        </NativeRouter>
      ) : (
        <Layout>
          {isLoaded ? (
            <>
              <Text>{translate('pickDirectoryExplanations')}</Text>
              <Button onPress={pickDirectory}>
                {translate('pickDirectory')}
              </Button>
            </>
          ) : (
            <Text>Loading…</Text>
          )}
        </Layout>
      )}
    </PaperProvider>
  );
};

export default App;
