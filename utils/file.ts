import _ from 'lodash';
import {FileStat} from 'react-native-file-access';

export interface PagerItem {
  chapter: string;
  page: string;
  uri: string;
  bgm: string | null;
  se: string[];
  voice: string | null;
}

export interface ScriptManifest {
  version: string;
  lang: string;
  soundDir?: string | undefined;
  title: string;
  story?: string | undefined;
  art?: string | undefined;
  soundScript?: string | undefined;
  translations: {[key: string]: [{name: string; url: string | null}]};
  volumes: {[key: string]: string[] | undefined};
  chapters: {
    [key: string]: {
      title: {[key: string]: string | null};
      pages: [
        {
          page: string;
          bgm: string | null;
          se: string[];
          voice: boolean;
        },
      ];
    };
  };
}

export const compareFiles = (a: FileStat, b: FileStat) => {
  if (a.filename > b.filename) {
    return 1;
  }
  if (a.filename < b.filename) {
    return -1;
  }
  return 0;
};

export const prettifyMusicFolderName = (folderName: string) => {
  return _.startCase(
    folderName
      .replace(/sound/g, '')
      .replace(/-/g, ' ')
      .replace(/_/g, ' ')
      .trim(),
  );
};
