﻿import React, {useEffect, useRef, useState} from 'react';
import Layout from '../components/Layout';
import MangaLink from '../components/MangaLink';
import {AppState, Linking, StyleSheet} from 'react-native';
import {Button, Dialog, Divider, Portal, Text} from 'react-native-paper';
import {translate} from '../../utils/localize';
import {FileStat, FileSystem} from 'react-native-file-access';
import _ from 'lodash';
import DeviceInfo from 'react-native-device-info';
import MusicBoxLink from '../components/MusicBoxLink';
import {compareFiles} from '../../utils/file';
import {SDCardDir} from '../../utils/preferences';

const s = StyleSheet.create({
  link: {
    color: '#ff9800',
  },
  divider: {
    marginTop: 20,
    marginBottom: 20,
  },
});

interface HomepageProps {
  packsDir: string;
}

const Homepage = ({packsDir}: HomepageProps) => {
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);
  const [visible, setVisible] = useState(false);
  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);
  const [directories, setDirectories] = useState<FileStat[]>([]);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        scanDirs();
      }

      appState.current = nextAppState;
      setAppStateVisible(appState.current);
    });

    return () => {
      subscription.remove();
    };
  }, []);

  const scanDirs = () => {
    FileSystem.statDir(SDCardDir + packsDir).then(result => {
      const directoriesFound: FileStat[] = [];
      if (result !== undefined && result.length > 0) {
        for (let index in result) {
          const item = result[index];
          if (item.type === 'directory') {
            directoriesFound.push(item);
          }
        }
      }
      directoriesFound.sort(compareFiles);
      setDirectories(directoriesFound);
    });
  };

  useEffect(() => {
    scanDirs();
  }, []);

  return (
    <Layout title={'Sound Manga Reader'} moreInfo={() => showDialog()}>
      <Text variant="titleLarge">{translate('mangaList')}</Text>

      {!_.isEmpty(directories) &&
        Object.values(directories).map(directory => (
          <MangaLink
            key={'manga-' + directory.filename}
            packsDir={packsDir}
            folder={directory.filename}
            appState={appState.current}
          />
        ))}

      <Text variant="bodyMedium">
        {translate('addMoreManga', {folder: packsDir + '/'})}
      </Text>
      <Text variant="bodyMedium">{translate('mangaAlreadyThere')}</Text>

      <Divider style={s.divider} />

      <Text variant="titleLarge">{translate('musicBox')}</Text>
      {!_.isEmpty(directories) &&
        Object.values(directories).map(directory => (
          <MusicBoxLink
            key={'music-' + directory.filename}
            packsDir={packsDir}
            folder={directory.filename}
          />
        ))}

      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>
            Sound Manga Reader, {translate('version')} {DeviceInfo.getVersion()}
          </Dialog.Title>
          <Dialog.Content>
            <Text variant="titleLarge">{translate('credits')}</Text>
            <Text>
              <Text
                style={s.link}
                onPress={() => {
                  Linking.openURL(
                    'https://gitlab.com/papjul/sound-manga-reader',
                  );
                }}>
                Sound Manga Reader
              </Text>
              {translate('projectAndContributors')}.
            </Text>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>{translate('done')}</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Layout>
  );
};

export default Homepage;
