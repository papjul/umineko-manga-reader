import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  useColorScheme,
  View,
} from 'react-native';
import {Appbar} from 'react-native-paper';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginBottom: 60, // FIXME: Check why we need to do this
  },
});

interface LayoutProps {
  children: React.ReactNode;
  title?: string | undefined;
  goBack?: (() => void) | undefined;
  moreInfo?: (() => void) | undefined;
}

const Layout = ({children, title, goBack, moreInfo}: LayoutProps) => {
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <Appbar.Header>
        {goBack && <Appbar.BackAction onPress={goBack} />}
        <Appbar.Content title={title ? title : 'Sound Manga Reader'} />
        {moreInfo && (
          <Appbar.Action icon="information-outline" onPress={moreInfo} />
        )}
      </Appbar.Header>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.container}>{children}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Layout;
