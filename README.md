![](screenshots/mangaList-lightTheme.png)
![](screenshots/chapterList-lightTheme.png)

![](screenshots/mangaList-darkTheme.png)
![](screenshots/chapterList-darkTheme.png)

![](screenshots/readerScreen.png)

# Description

Sound Manga Reader is a comic reader mobile app made with React Native.
This project is totally offline and runs on Android.

It adds enhanced features during the reading such as:
- Background Music
- Sound Effects
- Dub (voices for dialogs)

The developer would like to thank Goldsmith user from YouTube who inspired him to make this project.


# Installation instructions
- Download the app from the releases page (currently, only Android is available)
- Allow unknown sources to install the APK on your device
- The app itself contains no assets (manga) by default. You need to download them.


# Download assets
Fans make "sound manga packs" that can be installed on your Android device and then be read with this app.
They are split in (at least) two packs:
- One common pack that contains all BGM and SE assets that can be shared between multiple packs (for example, )
You're on your own to find websites to download them.

Note: Uninstalling the app will remove all assets from your Android internal storage, be careful!

# Development

```
npm start
```

# Production

```
cd android/
./gradlew clean
./gradlew bundleRelease
npm start
npx react-native run-android --mode=release
```

Get the APK from `android/app/build/outputs/apk/release/`.

# TODO

I don't have time right now but the next main focus is to change scripts based on chapter, so that one would only have to download the script and musics, and then chapters could be downloaded from Mangadex or other website.

Improvements:
- [Bug] Fix metadata not displayed for Opus files
- Show the music currently being played in Info on Chapter reading page
- Improve the musicbox
- Add ability to read assets from a specific folder? (for example, request permission to external storage)
- Make a script that can generate an "empty" .json based on the images found, for scripters
- Save settings + pages read
  
Platforms:
- Windows - Crash on start. Cannot find React Native Photo View. Looks like react-native.config.js is ignored. Find an audio library that is compatible with Windows and maintained (react-native-video?).
- iOS - Starts in simulator. Needs to be tested with packages unzipped. Needs all audios to be converted because Apple decided to support neither Vorbis or Opus audio codec. Help needed regarding the .IPA. Priority was lowered due to this platform being too jailed.
- MacOS - Work not started
- Linux with React Native Web?

Assets:
- Complete episode 8
- More voices
- Complete Tsubasa
- More languages (help wanted)
